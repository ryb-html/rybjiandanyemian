function dis_detail_more(obj_id,obj_id_taga){
    var obj = document.getElementById(obj_id);
    var detail_more_obj = document.getElementById(obj_id_taga);
    if(obj.style.display == 'none'){
        obj.style.display = 'block';
        detail_more_obj.innerHTML = '点击收起<br /><img src="../img/detail-more.png" />';
    }else{
        obj.style.display = 'none';
        detail_more_obj.innerHTML = '点击展开<br /><img src="../img/detail-stop.png" />';
    }
}
function change_detail_trip(){
    $("#mytab_li_1").removeClass('active');
    $("#mytab_li_2").addClass('active');
    $("#abstract").removeClass('tab-pane fade active in');
    $("#detailed").removeClass('tab-pane fade');
    $("#abstract").addClass('tab-pane fade');
    $("#detailed").addClass('tab-pane fade active in');
    $('html,body').animate({scrollTop:$('#myTab').offset().top-60},1000);
}
