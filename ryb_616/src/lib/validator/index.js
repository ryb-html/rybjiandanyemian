/**
 * ------------------------------------------------------------------
 * 表单验证类入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/4/13
 * ------------------------------------------------------------------
 */

import Validator from './validator'

export default Validator
