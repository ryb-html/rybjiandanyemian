/**
 * ------------------------------------------------------------------
 * 工具方法类
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 2017/3/20
 * ------------------------------------------------------------------
 */

import config from '@config'
import numeral from 'numeral'

class Utils {

  /**
   * storage存储
   * @param {string} key - 键
   * @param {string} value - 值
   * @return {boolean} - 是否存储成功
   */
  static setStorage (key, value) {
    if (!key || !value) return false
    window.localStorage.setItem(config.prefix + key, value)
    return true
  }

  /**
   * storage删除
   * @param {string} key - 键
   * @return {boolean} - 是否删除成功
   */
  static removeStorage (key) {
    if (!key) return false
    window.localStorage.removeItem(config.prefix + key)
    return true
  }

  /**
   * storage获取
   * @param {string} key - 键
   * @return {string} - 根据键查找到的值
   */
  static getStorage (key) {
    if (!key) return ''
    return window.localStorage.getItem(config.prefix + key)
  }

  /**
   * session存储
   * @param {string} key - 键
   * @param {string} value - 值
   * @return {boolean} - 是否存储成功
   */
  static setSession (key, value) {
    if (!key || !value) return false
    window.sessionStorage.setItem(config.prefix + key, value)
    return true
  }

  /**
   * session删除
   * @param {string} key - 键
   * @return {boolean} - 是否删除成功
   */
  static removeSession (key) {
    if (!key) return false
    window.sessionStorage.removeItem(config.prefix + key)
    return true
  }

  /**
   * session获取
   * @param {string} key - 键
   * @return {string} - 根据键查找到的值
   */
  static getSession (key) {
    if (!key) return ''
    return window.sessionStorage.getItem(config.prefix + key)
  }

  /**
   * 获取资源文件路径
   * @param {string} src - 资源路径
   * @return {string} - 返回资源绝对路径
   */
  static getResourcesPath (src) {
    if (!src) return src
    if (/^https?/.test(src)) return src
    return config.resourcesPath + src
  }

  /**
   * 更改页面标题
   * @description 设置页面title
   * @param {string} title - 要设置的title
   */
  static setTitle (title) {
    document.title = title
    let iframe = document.createElement('iframe')
    iframe.src = '/favicon.ico'
    iframe.style.display = 'none'
    iframe.onload = function () {
      setTimeout(function () {
        iframe.remove()
      }, 9)
    }
    document.body.appendChild(iframe)
  }

  /**
   * 数值格式化
   * @param number - 要格式化的数值
   * @return {*} - 格式化后的结果
   */
  static numberFormat (number) {
    if (typeof number !== 'number') {
      number = Number(number)
    }
    if (!number) return 0
    let numberLen = String(number).length
    if (numberLen >= 5 && numberLen < 7) {
      return numeral(number / 10000).format('0.00').replace(/\.0{2}$/, '') + '万'
    } else if (numberLen >= 7 && numberLen < 8) {
      return numeral(number / 1000000).format('0.00').replace(/\.0{2}$/, '') + '百万'
    } else if (numberLen >= 8 && numberLen < 9) {
      return numeral(number / 10000000).format('0.00').replace(/\.0{2}$/, '') + '千万'
    } else if (numberLen >= 9) {
      return numeral(number / 100000000).format('0.00').replace(/\.0{2}$/, '') + '亿'
    } else {
      return number
    }
  }
}

export default Utils
