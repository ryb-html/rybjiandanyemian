/**
 * ------------------------------------------------------------------
 * 工具方法类入口
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/23
 * ------------------------------------------------------------------
 */

import Utils from './utils'

export default Utils
