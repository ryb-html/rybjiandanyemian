/**
 * ------------------------------------------------------------------
 * 文件上传类入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 2017/3/24
 * ------------------------------------------------------------------
 */

import Upload from './upload'

export default Upload
