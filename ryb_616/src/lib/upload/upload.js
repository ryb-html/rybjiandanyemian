/**
 * ------------------------------------------------------------------
 * 文件上传类
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 2017/3/24
 * ------------------------------------------------------------------
 */

import { Toast } from 'mint-ui'
import http from '@http'
import axios from 'axios'

/**
 * @class
 * @description 文件上传类
 */

class Upload {

  /**
   * @param {Object} file - 要上传的文件
   * @param {Object} options - 选项
   * @param {String} [options.tokenType = 'normal'] - 要获取的token类型普通文件normal,视频文件video_fops
   * @param {String} options.fileType - 当前文件类型video,image
   * @param {String} options.businessName - 业务模块名称
   * @param {String} options.fileExtensionName - 后缀名
   * @param {Array} options.type - 合法文件类型
   * @param {Array} options.suffix - 合法文件后缀
   * @param {Array} options.min - 文件最小字节
   * @param {Array} options.max - 文件最大字节
   * @param {Function} options.onProgress - 上传进度事件
   */
  constructor (file, options) {
    this.file = file
    this.options = Object.assign({
      tokenType: 'normal',
      fileType: '',
      businessName: '',
      suffix: [],
      min: 0,
      max: 524288000,
      onProgress: () => {}
    }, options)
  }

  /**
   * getRandom
   * @description 生成随机字符串
   * @param {String} prefix - 字符串前缀
   * @return {String} - 生成的随机字符串
   */
  getRandom (prefix = '') {
    return prefix + (new Date()).getTime().toString(36) + '_' + Math.random().toString(36).substr(2)
  }

  /**
   * getToken
   * @description 获取token
   * @return {Promise}
   */
  getToken () {
    let url = {
      video_fops: 'getVideoToken',
      normal: 'getNormalToken'
    }
    let urlKeys = Object.keys(url)
    let self = this
    return new Promise((resolve, reject) => {
      let tokenType = self.options.tokenType
      if (urlKeys.indexOf(self.options.tokenType) === -1) reject('type参数错误')
      let params = {}
      let file = this.file
      if (tokenType === 'video_fops') {
        let key = this.getFileName(this.options.fileType, this.options.businessName, this.options.fileType === 'video' ? 'mp4' : /\.(.+)$/.exec(file.name)[1])
        this.saveKey = key
        params = {
          saveas: true,
          save_key: key
        }
      }
      http.post('qiniu/' + url[tokenType], params).then((res) => {
        resolve(res.data)
      }).catch((error) => {
        reject(error)
      })
    })
  }

  /**
   * getFileName
   * @param {String} fileType - 文件类型,video,image
   * @param {String} businessName - 业务模块名称
   * @param {String} fileExtensionName - 后缀名
   * @return {String} - 生成的文件名
   */
  getFileName (fileType, businessName, fileExtensionName) {
    if (arguments.length !== 3 || !fileType || !businessName || !fileExtensionName) return ''
    let date = new Date()
    let y = date.getFullYear()
    let m = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)
    let d = date.getDate() + 1 > 9 ? date.getDate() + 1 : '0' + (date.getDate() + 1)
    return `${fileType}/${businessName}/${y + m}/${d}/${this.getRandom() + '.' + fileExtensionName}`
  }

  /**
   * 文件上传
   * @param {String} token - 调用getToken方法获取到的token
   * @param {String} key - 文件名
   * @return {Promise}
   */
  upload (token, key) {
    let self = this
    return new Promise((resolve, reject) => {
      let formData = new FormData()
      formData.append('token', token)
      formData.append('file', self.file)
      formData.append('key', key)
      axios.post('http://upload-z1.qiniu.com', formData, {
        timeout: 0,
        onUploadProgress: (event) => {
          let total = event.total
          let loaded = event.loaded
          let loadProgress = Number(loaded / total * 100).toFixed(0)
          this.options.onProgress && this.options.onProgress(loadProgress, false, event)
        }
      }).then((res) => {
        let data = res.data
        if (data.key) {
          data.saveKey = this.saveKey
          resolve(data)
        } else {
          reject(data.error || data)
        }
      })
    })
  }

  /**
   * checkFileSize
   * @description 检查文件大小
   * @return {Boolean} - 文件大小是否在有效范围内
   */
  checkFileSize () {
    let size = this.file.size
    let max = Number(this.options.max)
    let min = Number(this.options.min)
    return size <= max && size >= min
  }

  /**
   * checkFileType
   * @description 检查文件类型
   * @return {boolean} - 文件类型是否合法
   */
  checkFileType () {
    let fileName = this.file.name.toLowerCase()
    let fileExtensionName = /\.[^.]+$/.exec(fileName)
    let type = fileExtensionName[0].toLocaleLowerCase()
    if (!this.options.suffix && !Array.isArray(this.options.suffix) && !this.options.suffix.length > 0) return false
    for (let i = 0; i < this.options.suffix.length; i++) {
      if (type.indexOf(this.options.suffix[i]) !== -1) {
        return true
      }
    }
    return false
  }

  /**
   * uploadSuccess
   * @description 文件路径转换成功后的回调
   * @callback uploadSuccess
   * @param {Object} data - 七牛接口返回信息
   */

  /**
   * uploadError
   * @description 文件路径转换成功后的回调
   * @callback uploadError
   * @param {String} msg - 七牛接口返回错误信息
   */

  /**
   * 开始上传
   * @param {uploadSuccess} success - 文件上传成功回调
   * @param {uploadError} error - 文件上传失败回调
   * @return {Boolean} - 文件格式、大小检查是否成功
   */
  start (success, error) {
    if (!this.file) {
      // Toast('请选择需要上传的文件')
      this.error()
      return false
    }
    if (!this.checkFileType()) {
      Toast('文件格式不正确,请上传' + (this.options.type.join('|') || this.options.suffix.join('|')) + '文件')
      this.error()
      return false
    }
    if (!this.checkFileSize()) {
      Toast('上传视频大小不能超过500M')
      this.error()
      return false
    }
    let self = this
    let file = this.file
    let key = self.getFileName(this.options.fileType, this.options.businessName, this.options.fileExtensionName || /\.(.+)$/.exec(file.name)[1])
    if (!key) return false
    let asyncUpload = async function (key) {
      let tokenResult = await self.getToken().catch((error) => {
        Toast(error.msg || error)
      })
      return await self.upload(tokenResult.qiniu_token, key)
    }
    asyncUpload(key).then((res) => {
      if (/^https?:\/\//.test(res.key)) {
        res.src = res.key
      } else {
        res.src = 'http://static.ryb.weixuehui.cn/' + res.key
      }
      success && success(res)
    }).catch((msg) => {
      error && error(msg)
    })
    return true
  }

  /**
   * 错误处理
   */
  error () {
    this.options.onProgress && this.options.onProgress(0, true)
  }
}
export default Upload
