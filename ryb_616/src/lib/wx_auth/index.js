/**
 * ------------------------------------------------------------------
 * 微信授权类入口
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/23
 * ------------------------------------------------------------------
 */

import WxAuth from './wx_auth'
import config from '@config'

let wxAuth = new WxAuth(config)

export default wxAuth
