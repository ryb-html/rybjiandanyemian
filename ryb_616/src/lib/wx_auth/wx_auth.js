/**
 * ------------------------------------------------------------------
 * 微信授权
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/23
 * ------------------------------------------------------------------
 */

import Utils from '@lib/utils'
import http from '@http'

class WxAuth {
  constructor (config) {
    this.config = config
    this.Utils = Utils
  }

  openAuthPage (redirectUri = encodeURIComponent(window.location.href)) {
    this.removeAccessToken()
    this.removeAuthCode()
    let authPageBaseUri = 'https://open.weixin.qq.com/connect/oauth2/authorize'
    let authParams = `?appid=${this.config.wx.appid}&redirect_uri=${redirectUri}&response_type=${this.config.wx.responseType}&scope=${this.config.wx.scope}#wechat_redirect`
    window.location.href = authPageBaseUri + authParams
  }

  setAuthCode (code) {
    if (!code) return false
    this.Utils.setSession('auth_code', code)
    return true
  }

  getAuthCode () {
    let codeValue = this.Utils.getSession('auth_code')
    if (!codeValue) return ''
    return codeValue
  }

  removeAuthCode () {
    this.Utils.removeSession('auth_code')
  }

  removeUrlCodeQuery () {
    let location = window.location
    let search = location.search
    if (search) {
      search = search.substr(1)
    }
    let href = location.origin
    let pathName = location.pathname
    if (pathName) {
      href += pathName
    }
    let searchArr = search.split('&').filter(item => {
      if (item.indexOf('code=') !== -1) {
        return false
      }
      if (item.indexOf('state=') !== -1) {
        return false
      }
      return true
    })
    if (searchArr.length > 0) {
      href += '?' + searchArr.join('&')
    }
    let hash = location.hash
    if (hash) {
      href += hash
    }
    window.location.href = href
  }

  setAccessToken (accessToken) {
    if (!accessToken) return false
    this.Utils.setStorage('access_token', accessToken)
    return true
  }

  getAccessToken () {
    return this.Utils.getStorage('access_token')
  }

  removeAccessToken () {
    this.Utils.removeStorage('access_token')
  }

  removeAccessTokenAndReload () {
    this.removeAccessToken()
    let url = window.location.href
    window.location.href = url
  }

  exchangeAccessToken () {
    return new Promise((resolve, reject) => {
      http.get('service/oauth', {
        params: {
          code: this.getAuthCode(),
          state: ''
        }
      }).then((res) => {
        let data = res.data
        let accessToken = data['x-jwt-token']
        if (accessToken) {
          this.setAccessToken(accessToken)
          resolve()
        } else {
          this.removeAuthCode()
          this.removeAccessToken()
          reject('用户登录失败')
        }
        this.removeAuthCode()
      }).catch((error) => {
        this.removeAuthCode()
        this.removeAccessToken()
        reject(error)
      })
    })
  }
}

export default WxAuth
