// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import MintUi from 'mint-ui'
import http from './http'
import api from './api'
import mixins from './mixins'
import filters from './filters'
import VueMomnet from 'vue-moment'
import componetns from './components'
import 'mint-ui/lib/style.min.css'
import './assets/stylus/app.styl'
import './assets/fonts/iconfont.css'

Vue.use(MintUi)
Vue.use(http)
Vue.use(api)
Vue.use(filters)
Vue.use(componetns)
Vue.use(VueMomnet)
Vue.config.productionTip = false
Vue.mixin(mixins.global)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
