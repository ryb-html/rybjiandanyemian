/**
 * ------------------------------------------------------------------
 * 全局过滤器入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 2017/3/24
 * ------------------------------------------------------------------
 */

import filters from './filters'

export default filters
