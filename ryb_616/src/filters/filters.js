/**
 * ------------------------------------------------------------------
 * 全局过滤器
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 2017/3/24
 * ------------------------------------------------------------------
 */

import Utils from '@lib/utils'

let filters = {
  install (Vue) {
    Vue.filter('getResourcesPath', Utils.getResourcesPath.bind(Utils))
    Vue.filter('numberFormat', Utils.numberFormat)
  }
}

export default filters
