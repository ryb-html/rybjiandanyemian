import Vue from 'vue'
import Router from 'vue-router'
import wxAuth from '@lib/wx_auth'
import url from 'url'
import querystring from 'querystring'
import { MessageBox } from 'mint-ui'

let SignIndex = () => System.import('@views/sign/index')
let SignAdd = () => System.import('@views/sign/add')
let SignRecord = () => System.import('@views/sign/record')
let VideoShowIndex = () => System.import('@views/video-show/index')
let VideoShowRule = () => System.import('@views/video-show/rule')
let VideoShowAdd = () => System.import('@views/video-show/add')
let VideoShowMy = () => System.import('@views/video-show/my')
let VideoTop = () => System.import('@views/video-show/top')
let WeekPicker = () => System.import('@views/video-show/week-picker')
let Error404 = () => System.import('@views/common/404')

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      redirect: {
        name: 'readingSignIndex'
      }
    },
    {
      name: 'readingSignIndex',
      path: '/reading/sign',
      meta: {
        auth: false,
        title: '21天亲子共读记'
      },
      component: SignIndex
    },
    {
      name: 'readingSignAdd',
      path: '/reading/sign/add',
      meta: {
        auth: true,
        title: '每日签到'
      },
      component: SignAdd
    },
    {
      name: 'readingSignRecord',
      path: '/reading/sign/record',
      meta: {
        auth: true,
        title: '签到记录'
      },
      component: SignRecord
    },
    {
      name: 'readingSignShareRecord',
      path: '/reading/sign/record/:openid',
      meta: {
        auth: false,
        title: '签到记录'
      },
      component: SignRecord
    },
    {
      name: 'videoShowIndex',
      path: '/video-show',
//    活动分享
      meta: {
        auth: true,
        title: 'The Muisc Class 亲子音乐大赛'
      },
      component: VideoShowIndex
    },
    {
      name: 'videoShowRule',
      path: '/video-show/rule',
      meta: {
        auth: true,
        title: '活动规则'
      },
      component: VideoShowRule
    },
    {
      name: 'videoShowAdd',
      path: '/video-show/add',
      // 上传视频
      meta: {
        auth: true,
        title: 'The Muisc Class 亲子音乐大赛'
      },
      component: VideoShowAdd
    },
    {
      name: 'videoShowTop',
      path: '/video-show/top',
      meta: {
        auth: true,
        title: '排行榜'
      },
      component: VideoTop
    },
    {
      name: 'videoShowWeekPicker',
      path: '/video-show/week-picker',
      meta: {
        auth: true,
        title: '每周精选'
      },
      component: WeekPicker
    },
    {
      name: 'videoShowMy',
      path: '/video-show/my/:openId?',
      meta: {
        auth: true,
        title: '我的视频'
      },
      component: VideoShowMy
    },
    {
      path: '*',
      meta: {
        auth: false,
        title: '页面不存在'
      },
      component: Error404
    }
  ]
})

// url中过滤code参数，防止同一code值多次调用
function urlCodeQueryFilter (code) {
  if (code) {
    wxAuth.setAuthCode(code)
    wxAuth.removeUrlCodeQuery()
  }
}

// 路由切换时检查是否已授权
function checkRouterAuth (to, from, next) {
  let authCode = wxAuth.getAuthCode()
  if ((!to.meta || !to.meta.auth) && !authCode) return true
  if (!authCode && !wxAuth.getAccessToken()) {
    wxAuth.openAuthPage(encodeURIComponent(window.location.href))
    return false
  } else if (authCode && !wxAuth.getAccessToken()) {
    wxAuth.exchangeAccessToken().then(() => {
      next()
    }).catch(error => {
      MessageBox('提示', error.msg || error)
    })
    return false
  }
  return true
}

router.beforeEach((to, from, next) => {
  let query = querystring.parse(url.parse(window.location.href).query)
  let code = query.code
  urlCodeQueryFilter(code)
  if (!code && !checkRouterAuth(to, from, next)) {
    return false
  }
  next()
})

export default router
