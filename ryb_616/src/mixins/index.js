/**
 * ------------------------------------------------------------------
 * 混合入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/24
 * ------------------------------------------------------------------
 */

import mixins from './mixins'

export default mixins
