/**
 * ------------------------------------------------------------------
 * 整合所有混合
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/24
 * ------------------------------------------------------------------
 */

import view from './view'
import global from './global'

export default {
  view,
  global
}
