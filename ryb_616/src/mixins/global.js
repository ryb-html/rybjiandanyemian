/**
 * ------------------------------------------------------------------
 * 全局混合
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/24
 * ------------------------------------------------------------------
 */

import Utils from '@lib/utils'

export default {
  created () {
    this.init && this.init()
  },
  methods: {
    getResourcesPath: Utils.getResourcesPath.bind(Utils)
  }
}
