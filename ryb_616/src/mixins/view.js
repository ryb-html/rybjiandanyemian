/**
 * ------------------------------------------------------------------
 * 页面级组件混合
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/24
 * ------------------------------------------------------------------
 */

import api from '@api'
import wx from 'weixin-js-sdk'
import { MessageBox, Toast } from 'mint-ui'
import wxAuth from '@lib/wx_auth'
import base64Url from 'base64-url'
import moment from 'moment'
import Utils from '@lib/utils'

export default {
  beforeRouteEnter (to, from, next) {
    if (to.meta && to.meta.title) {
      Utils.setTitle(to.meta.title)
    }
    let accessToken = wxAuth.getAccessToken()
    if (accessToken) {
      let exp = JSON.parse(base64Url.decode(accessToken.split('.')[1])).exp
      if (moment().unix() > exp && to.meta && to.meta.auth && !wxAuth.getAuthCode()) {
        wxAuth.openAuthPage()
        return false
      }
    }
    next()
  },
  beforeRouteUpdate () {
    this.init && this.init()
  },
  data () {
    return {}
  },
  methods: {
    initWxSdk () {
      return new Promise((resolve, reject) => {
        api.wxSdk.getWxSdkConfig({
          apis: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone'
          ],
          url: window.location.href.split('#')[0]
        }).then((res) => {
          let { config } = res.data
          wx.config(config)
          wx.ready(() => {
            this.wx = wx
            resolve(wx)
          })
          wx.error((error) => {
            MessageBox('微信js-sdk错误', error.errMsg)
            reject(error)
          })
        }).catch(error => {
          Toast(error.msg || error)
          reject(error)
        })
      })
    },

    /**
     * 微信分享
     * @param {object} wx - 微信sdk对象
     * @param {object} shareConfig - 分享配置信息
     * @param {string} shareConfig.title - 标题
     * @param {string} shareConfig.desc - 描述
     * @param {string} shareConfig.timeLineTitle - 分享到朋友圈标题
     * @param {string} shareConfig.link - 分享链接地址
     * @param {string} shareConfig.imgUrl - 封面地址
     * @param {function} shareConfig.success - 用户分享成功回调
     * @param {function} shareConfig.cancel - 用户分享成功回调
     */
    wxShare (wx, shareConfig) {
      let defaultShareConfig = {
        title: '21天亲子共读记',
        desc: '',
        link: window.location.href,
        timeLineTitle: '',
        imgUrl: 'http://static.ryb.weixuehui.cn/image/ryb_616/201703/30/j0u9vv4o_go0yyl0bh0xj21beti3u40a4i.png',
        success: () => {
          // Toast('分享成功')
        },
        cancel: () => {}
      }
      shareConfig = Object.assign(defaultShareConfig, shareConfig)
      wx.onMenuShareTimeline({
        ...shareConfig,
        title: shareConfig.timeLineTitle || shareConfig.title
      })
      wx.onMenuShareAppMessage(shareConfig)
      wx.onMenuShareQQ(shareConfig)
      wx.onMenuShareWeibo(shareConfig)
      wx.onMenuShareQZone(shareConfig)
    },

    /**
     * 微信图片预览
     * @param {string} current - 当前显示图片的http链接
     * @param {array} urls -  需要预览的图片http链接列表
     */
    wxPreviewImage (current, urls = []) {
      current = this.getResourcesPath(current)
      urls = urls.map(item => {
        return this.getResourcesPath(item)
      })
      this.wx && this.wx.previewImage({current, urls})
    },

    /**
     * 设置标题
     * @param {string} title - 标题
     */
    setTitle (title) {
      Utils.setTitle(title)
    }
  }
}
