/**
 * ------------------------------------------------------------------
 * 分享接口
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/27
 * ------------------------------------------------------------------
 */

import http from '@http'

let shareReadingSignIn = () => {
  return http.post('reading.sign_in/share')
}

let shareReadingVideo = () => {
  return http.post('reading.video/share')
}

export default {
  shareReadingSignIn,
  shareReadingVideo
}

