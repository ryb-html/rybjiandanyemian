/**
 * ------------------------------------------------------------------
 * 微信sdk接口
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/24
 * ------------------------------------------------------------------
 */

import http from '@http'

let getWxSdkConfig = params => {
  return http.post('service/jssdk', params)
}

export default {
  getWxSdkConfig
}
