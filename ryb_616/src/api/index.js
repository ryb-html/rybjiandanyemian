/**
 * ------------------------------------------------------------------
 * api入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/24
 * ------------------------------------------------------------------
 */

import wxSdk from './wx_sdk'
import share from './share'

let api = {
  wxSdk,
  share
}

api.install = Vue => {
  Vue.prototype.$api = api
}

export default api

