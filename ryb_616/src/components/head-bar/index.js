/**
 * ------------------------------------------------------------------
 * 头像组件入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/4/21
 * ------------------------------------------------------------------
 */

export { default } from './head-bar.vue'
