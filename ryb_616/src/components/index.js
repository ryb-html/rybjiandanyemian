/**
 * ------------------------------------------------------------------
 * 公共组件入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 2017/3/26
 * ------------------------------------------------------------------
 */

import SignDate from './sign-date'
import ImageList from './image-list'
import MessageBox from './message-box'
import Timeline from './timeline'
import Mask from './mask'

export default {
  install (Vue) {
    Vue.component('ryb-sign-date', SignDate)
    Vue.component('ryb-image-list', ImageList)
    Vue.component('ryb-message-box', MessageBox)
    Vue.component('ryb-timeline', Timeline)
    Vue.component('ryb-mask', Mask)
  }
}
