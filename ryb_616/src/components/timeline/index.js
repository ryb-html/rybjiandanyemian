/**
 * ------------------------------------------------------------------
 * 时间轴组件入口
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 2017/3/28
 * ------------------------------------------------------------------
 */

import Timeline from './timeline.vue'

export default Timeline
