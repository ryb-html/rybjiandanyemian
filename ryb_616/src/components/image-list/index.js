/**
 * ------------------------------------------------------------------
 * 图片列表组件入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/28
 * ------------------------------------------------------------------
 */

import ImageList from './image-list.vue'

export default ImageList
