/**
 * ------------------------------------------------------------------
 * 信息弹窗组件入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/28
 * ------------------------------------------------------------------
 */

import MessageBox from './message-box.vue'

export default MessageBox
