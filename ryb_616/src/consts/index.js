/**
 * ------------------------------------------------------------------
 * 系统常量入口文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/4/21
 * ------------------------------------------------------------------
 */

import CONSTS from './consts'
export default CONSTS
