/**
 * ------------------------------------------------------------------
 * 系统常量
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/4/21
 * ------------------------------------------------------------------
 */

export default {
  LIKE: 1, // 点赞
  NOT_LIKE: 0, // 未点赞
  ACT_TYPE_NURSERY: 1, // 活动类型，园所
  ACT_TYPE_FAMILY: 2, // 活动类型，家庭
  SEX_MAN: 1,
  SEX_WOMAN: 2
}
