/**
 * ------------------------------------------------------------------
 * http网络请求模块
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/23
 * ------------------------------------------------------------------
 */

import axios from 'axios'
import config from '@config'
import ERROR_CODE_LIST from './error_code_list'
import { Indicator, MessageBox } from 'mint-ui'
import wxAuth from '@lib/wx_auth'

let httpDefault = {
  baseURL: config.api.base,
  params: {
    _ajax: 1
  },
  data: {
    _ajax: 1
  }
}

if (process.env.NODE_ENV === 'production') {} else {
  httpDefault.params.dev = true
  httpDefault.data.dev = true
  httpDefault.headers = {
    'x-dev': true
  }
}
let http = axios.create(httpDefault)

http.interceptors.request.use(function (config) {
  Indicator.open()
  let accessToken = wxAuth.getAccessToken()
  if (accessToken) {
    config.headers = config.headers
      ? config.headers
      : {}
    config.headers['x-jwt-token'] = accessToken
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

http.interceptors.response.use(function (response) {
  Indicator.close()
  let data = response.data
  data.status = response.status
  let accessToken = response.headers['x-jwt-token']
  let localAccessToken = wxAuth.getAccessToken()
  if (accessToken && localAccessToken) {
    wxAuth.setAccessToken(accessToken)
  }
  try {
    if ((data.status >= 400 && data.status <= 502) || (data.code >= 400 && data.code <= 500) || ERROR_CODE_LIST[data.code] || ERROR_CODE_LIST[data.status]) {
      throw data
    }
  } catch (error) {
    let currentCodeError = ERROR_CODE_LIST[data.code] || ERROR_CODE_LIST[data.status]
    currentCodeError && currentCodeError.cb && currentCodeError.cb()
    if (currentCodeError && !currentCodeError.isShow) return Promise.reject(error)
    MessageBox('请求错误', `错误代码(${error.code}),错误内容(${error.msg || error})`)
    return Promise.reject(error)
  }
  try {
    if (data.code !== 0) {
      throw data
    }
  } catch (error) {
    return Promise.reject(error)
  }
  return data
}, function (error) {
  Indicator.close()
  return Promise.reject(error)
})

export default http
