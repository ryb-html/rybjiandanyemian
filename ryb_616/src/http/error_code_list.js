/**
 * ------------------------------------------------------------------
 * 错误码规则
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/24
 * ------------------------------------------------------------------
 */

import wxAuth from '@lib/wx_auth'

const ERROR_CODE_LIST = {
  11001: {
    msg: '用户未登录',
    isShow: false,
    cb: () => {
      if (!wxAuth.getAuthCode()) {
        wxAuth.openAuthPage()
      }
    }
  },
  11002: {
    msg: '会话过期',
    isShow: false,
    cb: () => {
      if (!wxAuth.getAuthCode()) {
        wxAuth.openAuthPage()
      }
    }
  },
  11003: {
    msg: '签名错误',
    isShow: false,
    cb: () => {
      if (!wxAuth.getAuthCode()) {
        wxAuth.openAuthPage()
      }
    }
  },
  11004: {
    msg: '用户授权出现错误',
    isShow: false,
    cb: () => {
      if (!wxAuth.getAuthCode()) {
        wxAuth.openAuthPage()
      }
    }
  }
}

export default ERROR_CODE_LIST
