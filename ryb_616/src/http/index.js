/**
 * ------------------------------------------------------------------
 * http模块主入口
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/23
 * ------------------------------------------------------------------
 */

import axios from './http'

axios.install = Vue => {
  Vue.prototype.$http = axios
}

export default axios
