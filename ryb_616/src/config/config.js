/**
 * ------------------------------------------------------------------
 * 配置文件
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/23
 * ------------------------------------------------------------------
 */

let config = {
  prefix: 'ryb_616_', // 存储前缀
  upload: '/upload', // 文件上传地址
  resourcesPath: 'http://static.ryb.weixuehui.cn/',
  wx: {
    appid: process.env.NODE_ENV === 'production' ? 'wxecaa08315ea847d2' : 'wxecaa08315ea847d2',
    responseType: 'code',
    scope: 'snsapi_userinfo'
  },
  api: {
    base: process.env.NODE_ENV === 'production' ? 'http://n53uzz.natappfree.cc/mp' : 'http://n53uzz.natappfree.cc/mp' // 接口地址
  }
}

export default config
