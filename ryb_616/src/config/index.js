/**
 * ------------------------------------------------------------------
 * 配置文件入口
 * @author SongJinDa <songjinda@rybbaby.com>
 * @date 17/3/23
 * ------------------------------------------------------------------
 */

import config from './config'

export default config
